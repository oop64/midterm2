/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.midterm2;

/**
 *
 * @author user1
 */
public class Minus extends Calculator {

    public Minus() {
        super();
        this.x = x;
        this.y = y;
    }

    @Override
    public void Calculating() {
        int sum;
        sum = x - y;
        outputMinus(sum);
    }

    private void outputMinus(int sum) {
        System.out.println(x + "-" + y + " = " + sum);
        System.out.println("--------Calculated--------");
        System.out.println();
    }

    public void Calculating(int operator) {
        double sum,a,b;
        a=x;
        b=y;
        sum = a / b;
        outputDivide(sum);
    }

    private void outputDivide(double sum) {
        System.out.println(x + "/" + y + " = " + sum);
        System.out.println("--------Calculated--------");
        System.out.println();
    }

}
